FROM ubuntu : 20.04 LTS
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y
RUN apt-get install apache2 -y 
RUN apt-get install apache2-utils -y 
RUN apt-get clean 
RUN sed -i 's/80/8086/g' /etc/apache2/ports.conf
EXPOSE 8086
ENTRYPOINT ["apache2ctl"]
CMD ["-DFOREGROUND"]

